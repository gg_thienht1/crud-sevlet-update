<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Edit Product</title>
   </head>
   <body>
 
      <h3>Edit Product</h3>
 
      <p style="color: red;">${errorString}</p>

         <form method="POST" action="${pageContext.request.contextPath}/update">
            <input type="hidden" name="idbook" value="${editbook.idbook}" />
            <table border="0">
               <tr>
                  <td>name</td>
                  <td><input type="text" name="name" value="${editbook.name}" /></td>
               </tr>
               <tr>
                  <td>description</td>
                  <td><textarea type="text" name="description" value="${editbook.description}" cols="25" rows="10"></textarea></td>
               </tr>
               <tr>
                  <td>author</td>
                  <td><input type="text" name="author" value="${editbook.author}" /></td>
               </tr>
               <tr>
                  <td colspan = "2">
                      <input type="submit" value="Submit" />
                      <a href="${pageContext.request.contextPath}/book">Cancel</a>
                  </td>
               </tr>
            </table>
         </form>

 
   </body>
</html>