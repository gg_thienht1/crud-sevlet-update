<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>book</title>
	</head>
	<body>
		<h1>ListBook </h1>
		<div>
			<input name="search" type="text">
			<input type="button" value="search">
			<a href="CreateBookServlet">add</a>
		</div>
		<div>
			<table border="1" cellpadding="5" cellspacing="1" >
       <tr>
       	<th>id</th>
          <th>Name</th>
          <th>description</th>
          <th>author</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
       <c:forEach items="${bookList}" var="book" >
          <tr>
             <td>${book.idbook}</td>
             <td>${book.name}</td>
             <td>${book.description}</td>
             <td>${book.author}</td>
             <td>
                <a href="${pageContext.request.contextPath}/update?idbook=${book.idbook}">Edit</a>
             </td>
             <td>
                <a href="${pageContext.request.contextPath}/delete?idbook=${book.idbook}">Delete</a>
             </td>
          </tr>
       </c:forEach>
    </table>
		</div>
	</body>
</html>