<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <form method="POST" action="${pageContext.request.contextPath}/CreateBookServlet">
         <table border="0">
            <tr>
               <td>Name</td>
               <td><input type="text" name="name" value="${book.name}" /></td>
            </tr>
            <tr>
               <td>Description</td>
               <td><input type="text" name="description" value="${book.description}" /></td>
            </tr>
            <tr>
               <td>author</td>
               <td><input type="text" name="author" value="${book.author}" /></td>
            </tr>
            <tr>
               <td colspan="2">                   
                   <input type="submit" value="Submit" />
                   <a href="book">Cancel</a>
               </td>
            </tr>
         </table>
      </form>
</body>
</html>