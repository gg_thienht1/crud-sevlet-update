package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import bean.BookEntity;
import util.MyData;


@WebServlet("/update")
public class UpdateBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UpdateBookServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/view/editProductView.jsp");
	        dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idbook=request.getParameter("idbook");
		String name = (String) request.getParameter("name");
        String description = (String) request.getParameter("description");
        String author = (String) request.getParameter("author");
        BookEntity book = new BookEntity();
        String errorString = null;
        try {
            MyData.updateBook(book);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        request.setAttribute("errorString", errorString);
        request.setAttribute("editbook", book);
        if (errorString != null) {
        	  request.setAttribute("errorString", errorString);
              // 
              RequestDispatcher dispatcher = request.getServletContext()
                      .getRequestDispatcher("/WEB-INF/view/deleteProductErrorView.jsp");
              dispatcher.forward(request, response);
        }

        else {
            response.sendRedirect(request.getContextPath() + "/book");
        }
    }
	}


