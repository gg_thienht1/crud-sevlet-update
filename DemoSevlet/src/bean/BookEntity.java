package bean;

public class BookEntity {
 
	private int idbook;
	private String name;
	private String description;
	private String author;
	
	public BookEntity() {
		// TODO Auto-generated constructor stub
	}

	public BookEntity(int idbook, String name, String description, String author) {
		super();
		this.idbook = idbook;
		this.name = name;
		this.description = description;
		this.author = author;
	}

	public BookEntity( String name, String description, String author) {
	
		this.name = name;
		this.description = description;
		this.author = author;
	}

	public int getIdbook() {
		return idbook;
	}

	public void setIdbook(int idbook) {
		this.idbook = idbook;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
}
